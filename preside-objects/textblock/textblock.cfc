/**
 * @dataManagerGroup      bscomponents
 * @datamanagergridfields internalTitle,title,slug,isActive
 * @labelField internalTitle
 * @siteFiltered true
 * @multilingual true
 * @useDrafts true
 */

component {
    property name="internalTitle" required=true maxLength=100 type="string" dbtype="varchar";
    property name="title" required=false default="" maxLength="200" type="string" dbtype="varchar";
    property name="headline" required=false default="" maxLength="200" type="string" dbtype="varchar";
    property name="textContent" required=true maxLength="1000" type="string" dbtype="varchar";
    property name="textRichtextContent" required=true type="string" dbtype="text" control="richeditor";
    property name="slug" type="string" dbtype="varchar" maxLength="50" required=true control="autoSlug" basedOn="internalTitle";
    property name="isActive" type="boolean" dbtype="boolean" required="false" default=true;
}
