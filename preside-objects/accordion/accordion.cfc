/**
 * @dataManagerGroup      bscomponents
 * @datamanagergridfields title,isActive
 * @labelField title
 * @siteFiltered true
 * @multilingual true
 * @useDrafts true
 */

component {
	property name="title" required=true maxLength=100 type="string" dbtype="varchar";
	property name="items" relationship="one-to-many" relatedto="accordion_item" relationshipkey="accordionid" control="oneToManyManager";
	property name="isActive" type="boolean" dbtype="boolean" required="false" default=true;
}
