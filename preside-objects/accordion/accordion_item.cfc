/**
 * @datamanagergridfields title,isActive,sortorder
 * @labelField title
 * @siteFiltered true
 * @multilingual true
 * @useDrafts true
 */

component {
	property name="title"   required=true maxLength=100 type="string" dbtype="varchar";
	property name="content"   required=true type="text" dbtype="text";
	property name="accordionid"	relationship="many-to-one"	relatedTo="accordion"	required=true;
	property name="sortorder" type="numeric" dbtype="integer" required=true maxLength="3" default="10";
	property name="isActive" type="boolean" dbtype="boolean" required="false" default=true;
}
