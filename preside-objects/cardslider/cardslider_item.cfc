/**
 * @datamanagergridfields title,subtitle,sortorder,isActive
 * @labelField title
 * @siteFiltered true
 * @multilingual true
 * @useDrafts true
 */

component {
	property name="title" required=true maxLength=100 type="string" dbtype="varchar";
    property name="subtitle" required=false default="" maxLength="100" type="string" dbtype="varchar";
    property name="cite" required=false default="" maxLength="100" type="string" dbtype="varchar";
    property name="citeperson" required=false default="" maxLength="100" type="string" dbtype="varchar";
    property name="personDesc" required=false default="" maxLength="100" type="string" dbtype="varchar";
    property name="detailText" required=false default="" maxLength="5000" type="string" dbtype="varchar";
    property name="mainImage" relationship="many-to-one" relatedTo="asset" required=false allowedTypes="image" ondelete="cascade-if-no-cycle-check" onupdate="cascade-if-no-cycle-check";
    property name="linkurl" required=false default="" maxLength="500" type="string" dbtype="varchar";
    property name="sortorder" type="numeric" dbtype="integer" required=true maxLength="3" default="10";
    property name="isActive" type="boolean" dbtype="boolean" required="false" default=true;
    property name="cardsliderid" relationship="many-to-one"	relatedTo="cardslider" required=true;
}
