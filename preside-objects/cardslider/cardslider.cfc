/**
 * @dataManagerGroup      bscomponents
 * @datamanagergridfields title,description,isActive
 * @labelField title
 * @siteFiltered true
 * @multilingual true
 * @useDrafts true
 */

component {
	property name="title" required=true maxLength=100 type="string" dbtype="varchar";
    property name="description" required=false default="" maxLength="500" type="string" dbtype="varchar";
    property name="items" relationship="one-to-many" relatedto="cardslider_item" relationshipkey="cardsliderid" control="oneToManyManager";
    property name="isActive" type="boolean" dbtype="boolean" required="false" default=true;
}
