component extends="preside.system.config.Config" {

	public void function configure( required struct config ) {
		var settings = arguments.config.settings ?: {};

		settings.assetmanager.derivatives.image_16to9 = {
			permissions     = "inherit"
			, inEditor        = true
			, transformations = [ { method="resize", args={ width=1280, height=720, maintainaspectratio=true, useCropHint=true } } ]
		};

		settings.assetmanager.derivatives.image_4to3 = {
			permissions     = "inherit"
			, inEditor        = true
			, transformations = [ { method="resize", args={ width=800, height=600, maintainaspectratio=true, useCropHint=true } } ]
		};

	}
}