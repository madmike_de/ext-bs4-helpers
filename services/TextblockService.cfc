component {
	property name="TextblockItemDAO" inject="presidecms:object:textblock";

	public any function get(event, rc, prc, args={}) {

		var ret = TextblockItemDAO.selectData(
			 selectFields = [ "id", "internalTitle", "title", "slug", "textContent", "textRichtextContent" ]
			,filter = { isActive=true, slug = arguments.slug }
        );
		return ret;
	}
	
}