component {
	property name="CardSliderItemDAO" inject="presidecms:object:cardslider_item";

	public any function get(event, rc, prc, args={}) {

		var ret = CardSliderItemDAO.selectData(
			 selectFields = [ "id", "title", "subtitle", "cite", "citeperson", "personDesc", "detailText", "mainImage", "linkurl", "sortorder", "isActive" ]
			,filter = { isActive=true, cardsliderid = arguments.id }
			,orderby = "sortorder ASC"
        );
		return ret;
	}
	
}