component {
	property name="AccordionItemDAO" inject="presidecms:object:accordion_item";

	public any function get(event, rc, prc, args={}) {

		var ret = AccordionItemDAO.selectData(
			 selectFields = [ "id", "title", "content" ]
			,filter = { isActive=true, accordionid = arguments.id }
			,orderby = "sortorder ASC"
        );
		return ret;
	}
	
}