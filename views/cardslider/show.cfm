<cfif args.items.recordcount>
    <div class="container">

        <p class="title text-center pt-4 mt-2">ÜBERSCHRIFT</p>

        <div class="owl-carousel relative my-4" id="owl-carousel">
            <cfoutput query="#args.items#">
                <div class="card">
                    <div class="position-relative">
                        #renderAsset(assetId=mainImage, args= { derivative="image_16to9", class="card-img-top", alt="#title#"})#
                    </div>    
                    <div class="card-body">
                        <p class="card-text font-weight-bold storyTxt storyCite text-center pt-4">&raquo;#trim(cite)#&laquo;</p>
                        <p class="text-center font-italic font-weight-light">#trim(citeperson)#</p>
                        <p class="text-center">#trim(personDesc)#</p>
                        <p class="text-center"><a href="#linkurl#" title="" class="btn btn-story w-50">MEHR</a></p>
                    </div>
                </div>
            </cfoutput>
        </div>
    </div>
</cfif>