<cfset vCount=1>
<cfoutput>
	<div class="container mb-4">
		<div class="accordion" id="accordion#args.id#">
			
			<cfloop query="#args.items#">
				
				<div class="card">
					<div class="card-header #args.cardHeaderAdditionalClass# pointer <cfif vCount!=1 or (vCount==1 and args.collapseFirst eq true)>collapsed</cfif>" id="heading#args.id#-#vCount#" data-toggle="collapse" data-target="##collapse#args.id#-#vCount#">
						<a href="##" class="collapseTitle" data-toggle="collapse" data-target="##collapse#args.id#-#vCount#" aria-expanded="true" aria-controls="collapse#args.id#-#vCount#">#args.items.title#<i class="fa float-right pt-1" aria-hidden="true"></i></a>
					</div>
					
					<div id="collapse#args.id#-#vCount#" class="collapse <cfif vCount==1 and !args.collapseFirst>show</cfif>" aria-labelledby="heading#args.id#-#vCount#" data-parent="##accordion#args.id#">
						<div class="card-body">
							#rendercontent("richeditor", args.items.content)#
						</div>
					</div>
				</div>
				
				<cfset vCOunt++ />
			</cfloop>
		</div>
	</div> 
</cfoutput>