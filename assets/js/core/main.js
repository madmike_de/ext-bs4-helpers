// implement your custom javascript here //
$(function() {
  //console.log('main.js loaded');

   //owl carousel
   $("#owl-carousel").owlCarousel(
    {
      items:3,
      loop:true,
      margin:9,
      autoplay:true,
      autoplayTimeout:1000,
      autoplayTimeout: 5000, 
      autoplayHoverPause:true,
      dots: false,
      nav: true,
      navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
      responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
            stagePadding:14                
        },
        768:{
            items:2,
            dots: true,
            nav: false,
            stagePadding:14                   
        },
        992:{
            items:3
        }
    }          
  }       
 );

});
