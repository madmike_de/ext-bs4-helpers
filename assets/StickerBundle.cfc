component output=false {

	public void function configure( bundle ) output=false {

		bundle.addAsset( id="js-ext_bs4_jquery", url="https://code.jquery.com/jquery-3.5.1.min.js" );
		bundle.addAsset( id="js-ext_bs4_popper", url="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" );
		bundle.addAsset( id="js-ext_bs4_bs", url="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" );
		bundle.addAsset( id="js-ext_bs4_owl", path="/js/core/owl.carousel.min.js" );
		bundle.addAsset( id="js-ext_bs4_main", path="/js/core/main.js" );
		bundle.addAsset( id="css-ext_bs4_bs", url="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" );
		bundle.addAsset( id="css-ext_bs4_fa", url="https://use.fontawesome.com/releases/v5.0.13/css/all.css" );
		bundle.addAsset( id="css-ext_bs4_owl.carousel.min", path="/css/core/owl.carousel.min.css" );
		bundle.addAsset( id="css-ext_bs4_owl.theme.default", path="/css/core/owl.theme.default.css" );
		bundle.addAsset( id="css-ext_bs4_main", path="/css/core/main.css" );

		_setExtraAttributes( bundle=arguments.bundle );
	}

	private void function _setExtraAttributes( required any bundle ){
		var excludedAssetIds = {
			async = ["js-ext_bs4_main"]
			, defer = ["js-ext_bs4_jquery"]
		};
		var allAssets = bundle.getAssets();

		for ( var asset in allAssets ) {

			if ( reFind( "\/js\/(specific|core)\/", asset ) EQ 1 ) {
				var extraAttributes = allAssets[ asset ].getExtraAttributes();

				if ( excludedAssetIds.async.find( asset ) EQ 0 ) {
					extraAttributes.async = "async";
				}

				if ( excludedAssetIds.defer.find( asset ) EQ 0 ) {
					extraAttributes.defer = "defer";
				}

				allAssets[ asset ].setExtraAttributes( extraAttributes );

			} else if ( find( "jq-", asset ) EQ 1 OR find( "js-", asset ) EQ 1 ) {

				var extraAttributes = allAssets[ asset ].getExtraAttributes();

				if ( excludedAssetIds.defer.find( asset ) EQ 0 ) {
					extraAttributes.defer = "defer";

					allAssets[ asset ].setExtraAttributes( extraAttributes );
				}
			}
		}
	}
}