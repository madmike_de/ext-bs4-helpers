# ext-bs4-helpers

## General

This is an extension for the [preside platform](https://www.preside.org/). The purpose of this extension is do extend your preside application with [Bootstrap 4](https://getbootstrap.com/) components in an easy peasy way. We currently use Bootstrap Version 4.5.0 with jQuery 3.5.1

## Installation

```
box install preside-ext-bs4-helpers
```

## Usage in your application

Use the assets shipped with the extension in your layout file:

```
	event.include( "css-ext_bs4_bs" )
		 .include( "css-ext_bs4_owl.carousel.min" )
		 .include( "css-ext_bs4_owl.theme.default" )
		 .include( "css-ext_bs4_main" )
		 .include( "css-ext_bs4_fa" )
		 .include( "js-ext_bs4_jquery" )
		 .include( "js-ext_bs4_popper" )
		 .include( "js-ext_bs4_bs" )
		 .include( "js-ext_bs4_owl" )
		 .include( "js-ext_bs4_main" );
```         


