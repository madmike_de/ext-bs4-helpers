component {
	property name="oTextblock" inject="TextblockService";

	public function show( event, rc, prc, args={} ) {
			// get textblock
			args.items = oTextblock.get(slug=args.slug);

			// render Textblock
			return renderView( view="textblock/show", args=args ); 
	}
}