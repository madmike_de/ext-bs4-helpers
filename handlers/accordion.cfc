component {
	property name="oAccordion" inject="AccordionService";

	public function show( event, rc, prc, args={} ) {
			// get Accordion			
			args.items = oAccordion.get(id=args.id);

			args.cardHeaderAdditionalClass = args.cardHeaderAdditionalClass ?: "";
			args.collapseFirst = args.collapseFirst ?: false;

			// render Accordion
			return renderView( view="accordion/show", args=args ); 
	}
}