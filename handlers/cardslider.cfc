component {
    property name="oCardSlider" inject="CardSliderService";

	public function show( event, rc, prc, args={} ) {
			// get Accordion			
			args.items = oCardSlider.get(id=args.id);

			//args.cardHeaderAdditionalClass = args.cardHeaderAdditionalClass ?: "";
			//args.collapseFirst = args.collapseFirst ?: false;

			// render Accordion
			return renderView( view="cardslider/show", args=args ); 
	}
}


